let num = +prompt('Choose a number:');
if (num > 5) {
    for (let i = 0; i <= num; i++) {
        if (i % 5 === 0 && i !== 0) {
            console.log(i);
        }
    }
} else {
    console.log('Sorry, no numbers');
}

// Optional
// let m = +prompt('Select number:');
// let n = +prompt('Select number:');
//
// while (m % 1 !== 0 || n % 1 !== 0 || m >= n) {
//     console.log('Something went wrong!');
//     m = +prompt('Please select number again:');
//     n = +prompt('Please select number again:');
// }